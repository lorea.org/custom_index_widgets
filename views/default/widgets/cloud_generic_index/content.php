<?php
$maintype = "object";
$subtype = $vars["entity"]->widget_subtype;
if (empty($subtype) || $subtype == 'All') {
	$subtype = ELGG_ENTITIES_ANY_VALUE;
}

if ($subtype == 'user') {$maintype='user';}
if ($subtype == 'group') {$maintype='group';}


$num_items = $vars['entity']->num_items;
if (!isset($num_items))
    $num_items = 20;

$created_time_limit = $vars['entity']->created_time_limit;
if (!isset($created_time_limit)  || $created_time_limit == 'all') {
    $created_time = ELGG_ENTITIES_ANY_VALUE;
}
elseif ($created_time_limit == 'day') {
    $created_time = time()-(3600*24);
}
elseif ($created_time_limit == 'week') {
    $created_time = time()-(3600*24*7);
}
elseif ($created_time_limit == 'month') {
    $created_time = time()-(3600*24*30);
}
	
$metadata_name = $vars['entity']->metadata_name;
if (!isset($metadata_name)) {
    $metadata_name = 'tags';
} else {
    $metadata_name = $metadata_name;
}

$threshold = $vars['entity']->threshold;
if (!isset($threshold))
    $threshold = 1;
	
$widget_group = $vars["entity"]->widget_group;
if (empty($widget_group)) $widget_group = ELGG_ENTITIES_ANY_VALUE;


elgg_push_context('tags');
$options = array('threshold' => $threshold,
		'limit' => $num_items,
		'type' => $maintype,
		'subtype' => $subtype,
		'tag_name' => $metadata_name,
		'created_time_lower' => $created_time,
		'container_guid' => $widget_group);

$body = elgg_view_tagcloud($options);
elgg_pop_context();

echo $body;
	
?>
